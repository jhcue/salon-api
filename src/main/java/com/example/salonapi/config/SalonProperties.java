package com.example.salonapi.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "salon.properties")
@Component
@Data
public class SalonProperties {
    private String name;
    private String address;
    private String city;
    private String state;
    private String zipcode;
    private String phone;
}
