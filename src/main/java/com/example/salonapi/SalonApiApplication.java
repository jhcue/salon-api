package com.example.salonapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableConfigurationProperties
public class SalonApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SalonApiApplication.class, args);
    }

}
