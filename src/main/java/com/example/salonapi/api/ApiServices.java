package com.example.salonapi.api;

import com.example.salonapi.db.SalonServiceDetailRepository;
import com.example.salonapi.model.SalonServiceDetail;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/services")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class ApiServices {

    private final SalonServiceDetailRepository salonServiceDetailRepository;

    @GetMapping(path = "/retrieveAvailableSalonServices", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(
            value = "retrieveAvailableServicesAPI",
            notes= "Retrieves a list of SalonServiceDetail",
            httpMethod = "GET",
            produces = "application/json")
    public ResponseEntity<List<SalonServiceDetail>> retrieveAvailableServices(
            @RequestParam(name = "p", required = false, defaultValue = "1")
            @ApiParam(name = "p", defaultValue = "1", type = "int", example = "5", value = "Page number") int p,
            @RequestParam(name = "s", required = false, defaultValue = "100")
            @ApiParam(name = "s", defaultValue = "100", type = "int", example = "25", value = "Page size") int s) {
        PageRequest pageRequest = PageRequest.of(p - 1, s, Sort.Direction.ASC, "name");
        Page<SalonServiceDetail> page = salonServiceDetailRepository.findAll(pageRequest);
        return ResponseEntity.ok(page.getContent());
    }

}
