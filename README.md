
# My notes for project

## Swagger2

How to install [here](https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)

### Vulnerabilities

#### implementation 'io.springfox:springfox-swagger2:2.9.2'

##### Vulnerabilities from dependencies:
[CVE-2021-20190](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-20190)
[CVE-2020-9548](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-9548)
[CVE-2020-9547](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-9547)
[CVE-2020-9546](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-9546)
[CVE-2020-8840](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-8840)
[CVE-2020-36188](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36188)
[CVE-2020-36186](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36186)
[CVE-2020-36184](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36184)
[CVE-2020-36182](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36182)
[CVE-2020-36180](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36180)
[CVE-2020-35491](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-35491)
[CVE-2020-25649](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-25649)
[CVE-2020-24616](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-24616)
[CVE-2020-14062](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-14062)
[CVE-2020-14060](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-14060)
[CVE-2020-11619](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-11619)
[CVE-2020-11112](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-11112)
[CVE-2020-10969](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-10969)
[CVE-2020-10673](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-10673)
[CVE-2019-20330](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20330)
[CVE-2019-17267](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-17267)
[CVE-2019-16942](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-16942)
[CVE-2019-14893](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14893)
[CVE-2019-14540](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14540)
[CVE-2019-14379](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14379)
[CVE-2019-12384](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12384)
[CVE-2018-19362](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-19362)
[CVE-2018-19360](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-19360)
[CVE-2018-14721](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14721)
[CVE-2018-14719](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14719)
[CVE-2018-10237](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-10237)

##### Solution

Use new version [3.0.0](https://mvnrepository.com/artifact/io.springfox/springfox-swagger2/3.0.0)